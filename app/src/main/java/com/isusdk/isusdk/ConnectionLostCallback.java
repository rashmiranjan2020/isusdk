package com.isusdk.isusdk;

public interface ConnectionLostCallback {
    public void connectionLost();
}
