package com.isusdk.isusdk;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.matm.matmsdk.MPOS.BluetoothServiceActivity;
import com.matm.matmsdk.MPOS.PosServiceActivity;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.BioAuthActivity;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    UsbManager musbManager;
    private UsbDevice usbDevice;
    String manufactureFlag = "";



    /**
     * There is No Radio button & EditText in Layout:
     * If you'll run this then you will get Error:
     */
    RadioButton rbCashWithdrawal, rbBalanceEnquiry, rb_mini;
    EditText etAmount;

    /**
     * --------------------------------------------
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**declare the msubManager as per the given doc  */
        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        /**After downloading teh json file call the JsonAssets class  */
        new JsonAssets(this);


    }


    /**
     * @return boolean
     * this function will check the biometricDevice Connection
     * @Author - Rashmi Ranjan
     */

    private Boolean biometricDeviceConnect() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        } else {
            for (UsbDevice device : connectedDevices.values()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }

                }
            }
        }
        return false;
    }


    /**
     * if the device is not connected then it will popup this AlertDialog message
     */
    private void deviceConnectMessgae() {
        androidx.appcompat.app.AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // finish();
                    }
                })
                .show();
    }


    /**
     * this Function will intent to ISU's sdk*/
    private void callAEPS2SDKApp() {
        SdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if (rbCashWithdrawal.isChecked()) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
        } else if (rbBalanceEnquiry.isChecked()) {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
        } else if (rb_mini.isChecked()) {
            SdkConstants.transactionType = SdkConstants.ministatement;
        }

        SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
        /**Declare the driverActivity in your manifest*/
        SdkConstants.DRIVER_ACTIVITY = "com.pax.pax_sdk_app.DriverActivity"; // path to your driver activity
        Intent intent = new Intent(MainActivity.this, BioAuthActivity.class);
        startActivity(intent);
    }

/**Use this function to connect your mAtm device */
    private void pairApp() {
        Intent intent = new Intent(MainActivity.this, BluetoothServiceActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH,
                            Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                    Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                } else {
                    startActivity(intent);
                }
            } else {
                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                    Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                } else {
                    startActivity(intent);
                }
            }
        }
        else {
            startActivity(intent);
        }
    }

    /**this function is intent to MatmSDK activity */
    private void callMATMSDKApp()   {
        SdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if (rbCashWithdrawal.isChecked())  {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = etAmount.getText().toString();
        }
        if (rbBalanceEnquiry.isChecked())   {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
            SdkConstants.transactionAmount = "0";
        }
        Intent intent = new Intent(MainActivity.this, PosServiceActivity.class);
        startActivity(intent);
    }
}
